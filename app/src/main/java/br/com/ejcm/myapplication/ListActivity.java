package br.com.ejcm.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class ListActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	}


	public void toAedesMap(View view) {
		Intent intent = new Intent(this, AedesMapActivity.class);
		startActivity(intent);
	}

	public void toGarbageMap(View view) {
		Intent intent = new Intent(this, GarbageMapActivity.class);
		startActivity(intent);
	}

	public void toRiverMap(View view) {
		Intent intent = new Intent(this, RiverMapActivity.class);
		startActivity(intent);
	}
}

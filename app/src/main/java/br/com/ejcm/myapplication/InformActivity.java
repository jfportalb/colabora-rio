package br.com.ejcm.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class InformActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inform);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	}

	public void towaterGraph(View view) {
		Intent intent = new Intent(this, WaterGraphActivity.class);
		startActivity(intent);
	}

	public void toDeslizamentoMap(View view) {
		Intent intent = new Intent(this, DeslizamentoActivity.class);
		startActivity(intent);
	}
}
